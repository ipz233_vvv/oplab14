#define _CRT_SECURE_NO_WARNING
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
�������� 2.

��������� ���������� ����� b ������ ����� � n=5 �������
� m=5 ���������. ������� ����� �� ����� � ������ �������, ������� �������
��������� ����� � ����� ������� ���� ������. ��������� ��������

a) �������� ������������ ������� � ���� �������;
b) �������� ���������� ������� � ���� �������;
c) �������� ������������������� �������� �������� ������� �����;
d) �������� ������� �������� ������� �������;
e) �������� ���� �������� ������� �������;
f) �������� ���� �������� �� �������� ��������� (�������: i<j);
*/
int main() {
	srand(time(NULL));
	int m[5][5];
	float average[5];
	int min = 1000, mini = -1, minj = -1;
	int max = -1, maxi = -1, maxj = -1;
	int dmul = 1;
	int dsum = 0;
	int dundersum = 0;
	for (int i = 0; i < 5; i++) {
		float sum = 0;
		for (int j = 0; j < 5; j++) {
			m[i][j] = rand() % 50;
			printf("%02d ",m[i][j]);
			sum += m[i][j];

			mini = (m[i][j] < min) ? i : mini;
			minj = (m[i][j] < min) ? j : minj;
			min = (m[i][j] < min) ? m[i][j] : min;

			maxi = (m[i][j] > max) ? i : maxi;
			maxj = (m[i][j] > max) ? j : maxj;
			max = (m[i][j] > max) ? m[i][j] : max;

			dsum += (i == j) ? m[i][j] : 0;
			dmul *= (i == j) ? m[i][j] : 1;

			dundersum += (i < j) ? m[i][j] : 0;
		}
		average[i] = sum / 5;
		printf("\n");
	}
	printf("Biggest element is %d at [%d,%d]\n", max, maxi, maxj);
	printf("Smallest element is %d at [%d,%d]\n", min, mini, minj);
	printf("Average of each row: \n");
	for (int i = 0; i < 5; i++) {
		printf(" Row %d: %.1f\n",i+1, average[i]);
	}
	printf("Product of main diagonal elements: %d\n", dmul);
	printf("Sum of main diagonal elements: %d\n", dsum);
	printf("Sum of elements under main diagonal: %d\n", dundersum);
	return 0;
}