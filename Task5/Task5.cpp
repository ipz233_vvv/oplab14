#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
�������� 5.

����� ����� ��������� ������ ��������� ��������
�� 1 �� 6 (����� k), � ���� ��������� � �������� �� 1 �� 5 (����� n). ʳ������
������, �������� ����������� � ������� �����, ����������� �� ��������
A[k] = rand()%10i + 50, �� i � ����� �������.
������� �� ����� ������� ���������� �����������, �� �������� � ������ �
���� � ��������� ������, � � �������� � ���� �� ���������� ����������

��� ������� ������ ���� ������ �� 1 � 3 ��������� � ��� ���������
�������?
*/
int main() {
	srand(time(NULL));
	int k, n;
	int m[6][5];
	int sum[5] = {0};
	printf("Enter k [1;6] and n [1,5]:\n");
	scanf("%d%d", &k, &n);
	for (int i = 0; i < k; i++) {
		printf("Settlement %d: ",i+1);
		for (int j = 0; j < n; j++) {
			m[i][j] = rand() % (10 * 3) + 50;
			printf("%02d ", m[i][j]);
			sum[j] += m[i][j];
		}
		printf("\n");
	}
	for (int i = 0; i < n; i++) {
		printf("Candidate %d got %d points\n", i + 1, sum[i]);
	}
	printf("\nCandidate 1 got %d points, candidate 3 got %d points\n", sum[0], sum[2]);
	return 0;
}