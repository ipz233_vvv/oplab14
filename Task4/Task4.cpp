#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
�������� 4.
������ ���� �������� ������������ �������
|\#####|
| \####|
|  \###|
|   \##|
|    \#|
|     \|

*/
int main() {
	srand(time(NULL));
	int n;
	int m[100][100];
	printf("Enter array size n (should be less or equal 100):\n");
	scanf("%d", &n);
	if (n > 100) {
		printf("m and n should be less or equal 100.\n");
	}
	else {
		int s = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				m[i][j] = rand() % 10;
				printf("%d ", m[i][j]);
				s += i < j ? m[i][j] : 0;
			}
			printf("\n");
		}
		printf("Sum: %d", s);
	}
	return 0;
}