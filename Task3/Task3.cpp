#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
�������� 3.
��������� ��������, ������������ ����� �����:
� ��������������� �������� ������;
� ����������� ������ ������ [N][M] � �������� ���������;
� ������� ������ ������ nim(n<N, m<M) � ������ � ���������(��� ����� ���������� �������� ����������� �������� �����);
� �������� ������ ��������������� �����, ����������� �� �������� [a, b], �� a � b(a<b) ��������� � ���������;
� �� ������ ���� � �������� ������ �������� �� �����

���������� ��������, ��� ���� �������� �������� ��������� �������
�� �������� ��������� �������� �������� ������������ ������
*/
int main() {
	int m1[100][100];
	int m2[10000];
	int n, m;
	printf("Enter array dimensions n and m (should be less or equal 100):\n");
	scanf("%d %d", &n, &m);
	srand(time(NULL));
	if (n > 100 || m > 100) {
		printf("m and n should be less or equal 100.\n");
	}
	else {
		for (int i = 0; i < m * n; i++) {
			m2[i] = rand() % (m * n);
			printf("%d ", m2[i]);
		}
		printf("\n\n");
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				m1[i][j] = m2[i * n + j];
				printf("%d ", m1[i][j]);
			}
			printf("\n");
		}
	}
	return 0;
}